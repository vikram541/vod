<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Gener Entity.
 */
class Gener extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'created_date' => true,
        'updated_date' => true,
    ];
}
