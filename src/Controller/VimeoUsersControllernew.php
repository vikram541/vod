<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * VimeoUsers Controller
 *
 * @property \App\Model\Table\VimeoUsersTable $VimeoUsers
 */
class VimeoUsersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('vimeoUsers', $this->paginate($this->VimeoUsers));
        $this->set('_serialize', ['vimeoUsers']);
    }

    /**
     * View method
     *
     * @param string|null $id Vimeo User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vimeoUser = $this->VimeoUsers->get($id, [
            'contain' => []
        ]);
        $this->set('vimeoUser', $vimeoUser);
        $this->set('_serialize', ['vimeoUser']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vimeoUser = $this->VimeoUsers->newEntity();
        if ($this->request->is('post')) {
            $vimeoUser = $this->VimeoUsers->patchEntity($vimeoUser, $this->request->data);
            if ($this->VimeoUsers->save($vimeoUser)) {
                $this->Flash->success(__('The vimeo user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The vimeo user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('vimeoUser'));
        $this->set('_serialize', ['vimeoUser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Vimeo User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vimeoUser = $this->VimeoUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vimeoUser = $this->VimeoUsers->patchEntity($vimeoUser, $this->request->data);
            if ($this->VimeoUsers->save($vimeoUser)) {
                $this->Flash->success(__('The vimeo user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The vimeo user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('vimeoUser'));
        $this->set('_serialize', ['vimeoUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Vimeo User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vimeoUser = $this->VimeoUsers->get($id);
        if ($this->VimeoUsers->delete($vimeoUser)) {
            $this->Flash->success(__('The vimeo user has been deleted.'));
        } else {
            $this->Flash->error(__('The vimeo user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
