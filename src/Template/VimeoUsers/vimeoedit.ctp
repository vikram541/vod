<div class="row">
     <div class="col-md-offset-11">
   
        <div class="dropdown">
            <button class="btn btn-warning dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?= __('Actions') ?>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="margin-left:-76px;">
                <li><?=
                    $this->Form->postLink(
                          $this->Html->tag('span', '', ['class' => 'fa fa-fw fa-trash-o', 'title' => 'Delete']) . "Delete API", ['action' => 'vimeodelete', $vimeoUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vimeoUser->id),'escape' => false]
                    )
                    ?></li>
                <li><?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-fw fa-backward', 'title' => 'Back']) . " Back to API", ['action' => 'vimeoindex'],['escape' => false]) ?></li>
            </ul>
        </div>

   
     </div>      
    <div class="col-md-12">
        <?= $this->Form->create($vimeoUser) ?>

        <fieldset>

            <legend><?= __('Edit Vimeo User') ?> 

            </legend>
            <?php
            echo '<div class="col-md-6">';
            echo $this->Form->input('cid', ['type' => 'text', 'label' => 'Client Id', 'required', 'class' => 'form-control']);
            echo '</div>';
            echo '<div class="col-md-6">';
            echo $this->Form->input('client_secret', ['type' => 'text', 'label' => 'Client Access Token Key', 'required', 'class' => 'form-control']);
            echo '</div>';
            echo '<div class="col-md-12">';
            echo $this->Form->input('client_access_token', ['type' => 'text', 'label' => 'Client Secret Key', 'required', 'class' => 'form-control']);
            echo '</div>';
            echo '<div class="pull-right col-md-3">';
            echo $this->Form->button('<i class="fa fa-fw fa-floppy-o"></i>Edit', ['type' => 'submit', 'class' => 'btnmargin btn btn-success pull-right', 'div' => false]);
            echo '</div>';
            ?>
        </fieldset>
        <?= $this->Form->end() ?>

    </div>
</div>

