
<?php
echo '<div style="margin-top:65px;">';
//    echo '<div class="col-md-4 filmimg">';
//    echo $this->Html->image('flim3.png',['alt'=>'no image','class'=>'img img-responsive']);    
//    echo '</div>';


echo '<div class="col-md-offset-1 col-md-9">';
echo $this->Flash->render();
echo '<div class="back">';
echo '<label><h1><i class="fa fa-sign-in"></i> Sign In</h1></label>';
echo $this->Form->create(null, ['horizontal' => true]);
echo $this->Form->input('username', ['type' => 'text', 'placeholder' => 'Enter User Name', 'required']);
echo $this->Form->input('password', ['type' => 'password', 'placeholder' => 'Enter Password', 'required']);
echo $this->Form->input('remember', ['type' => 'checkbox', 'id' => 'remember', 'placeholder' => 'Enter User Name']);
echo $this->Form->submit('Log In', ['class' => 'btn btn-primary btn-block']);
echo '<div class="form-group>';
echo '<span class="notmember"><label>Not Member</label> ';
echo $this->Html->link('Sign Up ?', ['controller' => 'Users', 'action' => 'signup', '_full' => true], ['class' => 'anchor1']);
echo '</span>';
echo '<span class="col-md-offset-3">';
echo '<span id="status"></span>';
echo '<fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>';
echo '</span>';
echo '</div>';
echo $this->Form->end();
echo '</div>';
echo '</div>';
echo '</div>';

?>

<script type="text/javascript">

  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
              // alert(JSON.stringify(response));
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
     $('#status').html('Please log ' +
        'into this app.');
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
     $('#status').html('Facebook '+'   ');
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '964415850246078',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
  //  console.log('Welcome!  Fetching your information.... ');
//  var url      = window.location;
//  alert(url);
    var client_id=$('#client_id').val();
    //alert(client_id);
    FB.api('/me', function(response) {
        
         $.ajax({
            url: 'http://localhost/videoapi/Users/fblogin',
            type: 'post',
            data: {'userdata':JSON.stringify(response),'client_id':client_id},
            success: function (data) {
            //alert(data);
                //$('#target').html(data.msg);
            },
            error : function (data){
                alert(data);
            }
            
        });
      console.log('Successful login for: ' + response.name);
     $('#status').html(
      'Thanks for logging in, ' + JSON.stringify(response) + '!');
    });
  }

</script>
