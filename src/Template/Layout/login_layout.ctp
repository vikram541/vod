<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>TechPhant</title>
        <?= $this->Html->meta('icon') ?>
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('font-awesome.min.css') ?>
        <?= $this->Html->css('plugins/morris.css') ?>
        <?= $this->Html->css('sb-admin.css') ?>
        <?= $this->Html->css('custom.css') ?>
        <?= $this->Html->script('jquery.js') ?>  
        <?php $this->Html->script('bootstrap.min.js') ?>
        <?php /* $this->Html->script('plugins/morris/morris-data.js')*/ ?>
        <?= $this->Html->script('plugins/morris/raphael.min.js') ?>
        <?= $this->Html->script('script.js') ?>
        
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body class="backimg">
        <div id="wrapper">
             <div class="container-fluid">
                    <?= $this->fetch('content') ?>
                </div>    
            
        </div>
    </body>
</html>