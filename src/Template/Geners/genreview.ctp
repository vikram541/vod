<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Gener'), ['action' => 'edit', $gener->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Gener'), ['action' => 'delete', $gener->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gener->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Geners'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gener'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="geners view large-10 medium-9 columns">
    <h2><?= h($gener->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($gener->name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($gener->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created Date') ?></h6>
            <p><?= h($gener->created_date) ?></p>
            <h6 class="subheader"><?= __('Updated Date') ?></h6>
            <p><?= h($gener->updated_date) ?></p>
        </div>
    </div>
</div>
