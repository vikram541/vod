<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $gener->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $gener->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Geners'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="geners form large-10 medium-9 columns">
    <?= $this->Form->create($gener) ?>
    <fieldset>
        <legend><?= __('Edit Gener') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('created_date');
            echo $this->Form->input('updated_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
