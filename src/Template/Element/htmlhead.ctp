<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>TechPhant</title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('font-awesome.min.css') ?>
    <?= $this->Html->css('plugins/morris.css') ?>
    <?= $this->Html->css('sb-admin.css') ?>
    <?= $this->Html->css('//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css') ?>
    <?= $this->Html->css('//cdn.datatables.net/responsive/1.0.6/css/dataTables.responsive.css') ?>
    <?= $this->Html->css('//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') ?>
    <?= $this->Html->css('tokenfield/tokenfield-typeahead.css') ?>
    <?= $this->Html->css('tokenfield/bootstrap-tokenfield.css') ?>

     <?= $this->Html->script('jquery.js') ?>
        <?= $this->Html->script('bootstrap.min') ?>
        <?= $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.js') ?>
        <?= $this->Html->script('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js') ?>
        <?= $this->Html->script('//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js') ?>
        <?= $this->Html->script('//cdn.datatables.net/responsive/1.0.6/js/dataTables.responsive.js') ?>
        <?= $this->Html->script('//code.jquery.com/ui/1.10.3/jquery-ui.js') ?>
        <?= $this->Html->script('tokenfield/bootstrap-tokenfield.js') ?>
        
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <style type="text/css">
        .btnmargin{
            margin-top: 5%;
        }
        .btnmrleft{
            margin-left:10px;
        }
    </style>
</head>
