<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        <li class="active">
            <?php
            echo $this->Html->link(
                    $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-dashboard']) . " Dashboard", ['controller' => 'Users', 'action' => 'Dashboard'], ['escape' => false]
            );
            ?>
        </li>
        <li>
            <?php
            echo $this->Html->link(
                    $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-bar-chart-o']) . " Upload Video", ['controller' => 'videos', 'action' => 'add'], ['escape' => false]
            );
            ?>

        </li>
        <li>
            <?php
            echo $this->Html->link(
                    $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-table']) . " View Video Details", ['controller' => 'videos', 'action' => 'index'], ['escape' => false]
            );
            ?>

        </li>
        <li>
            <?php
            echo $this->Html->link(
                    $this->Html->tag('i', '', ['class' => 'fa fa-support']) . " Support", ['controller' => 'Users', 'action' => 'support'], ['escape' => false]
            );
            ?>  
        </li>
          <?=$this->element('settings/commonsettings')?>


    </ul>
</div>