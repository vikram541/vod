<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">

        <li class="active">
            <?php
            echo $this->Html->link(
                    $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-dashboard']) . " Dashboard", ['controller' => 'Users', 'action' => 'Dashboard'], ['escape' => false]
            );
            ?>
        </li>
        <li>
            <a href="javascript:;" data-toggle="collapse" data-target="#master"><i class="fa fa-fw fa-dashcube"></i> Master Entry Form<i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="master" class="collapse">
                <li>
                    <?php
                    echo $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-angle-right']) . "Vimeo API ", ['controller' => 'vimeo_users', 'action' => 'vimeoindex'], ['escape' => false]
                    );
                    ?>
                </li>
                <li>
                    <?php
                    echo $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-angle-right']) . "Video Genre", ['controller' => 'geners', 'action' => 'genreindex'], ['escape' => false]
                    );
                    ?>
                </li>
                <li>
                    <?php
                    echo $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-angle-right']) . "Video Topic", ['controller' => 'Users', 'action' => 'videotopic'], ['escape' => false]
                    );
                    ?>
                </li>
                <li>
                    <?php
                    echo $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-angle-right']) . "Video Tags", ['controller' => 'Users', 'action' => 'videotags'], ['escape' => false]
                    );
                    ?>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" data-toggle="collapse" data-target="#video"><i class="fa fa-fw fa-dashcube"></i> Video Section<i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="video" class="collapse">
                <li>
                    <?php
                    echo $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-bar-chart-o']) . " Upload Video", ['controller' => 'videos', 'action' => 'add'], ['escape' => false]
                    );
                    ?>

                </li>
                <li>
                    <?php
                    echo $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-table']) . " View Video Details", ['controller' => 'videos', 'action' => 'index'], ['escape' => false]
                    );
                    ?>

                </li>
            </ul>
        </li>

        <?= $this->element('settings/commonsettings') ?>
        <li>
            <?php
            echo $this->Html->link(
                    $this->Html->tag('i', '', ['class' => 'fa fa-area-chart']) . " Reports", ['controller' => 'Users', 'action' => 'reports'], ['escape' => false]
            );
            ?>  
        </li>

    </ul>
</div>