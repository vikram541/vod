<div id="page-sidebar">
    <div class="scroll-sidebar">
        <ul id="sidebar-menu">
    <li class="header"><span>Overview</span></li>
    <li><a href="<?=$this->Url->build('/creator/dashboard',["controller" => "Users","action" => "dashboard","foo" => "bar"]);?>" title="Admin Dashboard">
            <i class="glyph-icon icon-linecons-tv"></i>
            <span>Creator dashboard</span>
        </a>
    </li>
    <li class="divider"></li>
    <li><a href="<?=$this->Url->build('/creator/addfilm',["controller" => "Users","action" => "addfilm","foo" => "bar"]);?>" title="Admin Dashboard">
            <i class="glyph-icon icon-cloud-upload"></i>
            <span>Add New Film</span>
        </a>
    </li>
    <li class="header"><span>Settings</span></li>
  
    <li><a href="<?=$this->Url->build('/creator/account',["controller" => "Users","action" => "dashboard","foo" => "bar"]);?>" title="Admin Dashboard">
            <i class="glyph-icon icon-gears"></i>
            <span>Account</span>
        </a>
    </li>
    <li class="divider"></li>
    <li><a href="<?=$this->Url->build('/creator/support',["controller" => "Users","action" => "addfilm","foo" => "bar"]);?>" title="Admin Dashboard">
            <i class="glyph-icon icon-globe"></i>
            <span>Support</span>
        </a>
    </li>
</ul><!-- #sidebar-menu -->
    </div>
</div>