<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Video'), ['action' => 'edit', $video->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Video'), ['action' => 'delete', $video->id], ['confirm' => __('Are you sure you want to delete # {0}?', $video->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Videos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Video'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="videos view large-10 medium-9 columns">
    <h2><?= h($video->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Video Title') ?></h6>
            <p><?= h($video->video_title) ?></p>
            <h6 class="subheader"><?= __('Video Director') ?></h6>
            <p><?= h($video->video_director) ?></p>
            <h6 class="subheader"><?= __('Video Synopsis') ?></h6>
            <p><?= h($video->video_synopsis) ?></p>
            <h6 class="subheader"><?= __('Country') ?></h6>
            <p><?= h($video->country) ?></p>
            <h6 class="subheader"><?= __('Film Festival') ?></h6>
            <p><?= h($video->film_festival) ?></p>
            <h6 class="subheader"><?= __('Language') ?></h6>
            <p><?= h($video->language) ?></p>
            <h6 class="subheader"><?= __('Video Logline') ?></h6>
            <p><?= h($video->video_logline) ?></p>
            <h6 class="subheader"><?= __('Video Cast') ?></h6>
            <p><?= h($video->video_cast) ?></p>
            <h6 class="subheader"><?= __('Video Link') ?></h6>
            <p><?= h($video->video_link) ?></p>
            <h6 class="subheader"><?= __('Video File Type') ?></h6>
            <p><?= h($video->video_file_type) ?></p>
            <h6 class="subheader"><?= __('Channel') ?></h6>
            <p><?= h($video->channel) ?></p>
            <h6 class="subheader"><?= __('Video Trailer Url') ?></h6>
            <p><?= h($video->video_trailer_url) ?></p>
            <h6 class="subheader"><?= __('Video Twitter') ?></h6>
            <p><?= h($video->video_twitter) ?></p>
            <h6 class="subheader"><?= __('Video Fbpage') ?></h6>
            <p><?= h($video->video_fbpage) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($video->id) ?></p>
            <h6 class="subheader"><?= __('Userid') ?></h6>
            <p><?= $this->Number->format($video->userid) ?></p>
            <h6 class="subheader"><?= __('Countryid') ?></h6>
            <p><?= $this->Number->format($video->countryid) ?></p>
            <h6 class="subheader"><?= __('Video Length') ?></h6>
            <p><?= $this->Number->format($video->video_length) ?></p>
            <h6 class="subheader"><?= __('Video Size') ?></h6>
            <p><?= $this->Number->format($video->video_size) ?></p>
            <h6 class="subheader"><?= __('Total Like') ?></h6>
            <p><?= $this->Number->format($video->total_like) ?></p>
            <h6 class="subheader"><?= __('Total Share') ?></h6>
            <p><?= $this->Number->format($video->total_share) ?></p>
            <h6 class="subheader"><?= __('Total Viewed') ?></h6>
            <p><?= $this->Number->format($video->total_viewed) ?></p>
            <h6 class="subheader"><?= __('Price') ?></h6>
            <p><?= $this->Number->format($video->price) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($video->created) ?></p>
            <h6 class="subheader"><?= __('Modidfied') ?></h6>
            <p><?= h($video->modidfied) ?></p>
        </div>
        <div class="large-2 columns booleans end">
            <h6 class="subheader"><?= __('Is Hd Sd') ?></h6>
            <p><?= $video->is_hd_sd ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('Is Paid') ?></h6>
            <p><?= $video->is_paid ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('Is Approved') ?></h6>
            <p><?= $video->is_approved ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('Need Signup') ?></h6>
            <p><?= $video->need_signup ? __('Yes') : __('No'); ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Genres') ?></h6>
            <?= $this->Text->autoParagraph(h($video->genres)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Topics') ?></h6>
            <?= $this->Text->autoParagraph(h($video->topics)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Tags') ?></h6>
            <?= $this->Text->autoParagraph(h($video->tags)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Video Year') ?></h6>
            <?= $this->Text->autoParagraph(h($video->video_year)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Video Description') ?></h6>
            <?= $this->Text->autoParagraph(h($video->video_description)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Video Referal') ?></h6>
            <?= $this->Text->autoParagraph(h($video->video_referal)) ?>
        </div>
    </div>
</div>
